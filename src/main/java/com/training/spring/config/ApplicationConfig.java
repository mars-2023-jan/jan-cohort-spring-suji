package com.training.spring.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

@Configuration
@ComponentScan("com.training.spring")
public class ApplicationConfig {

	@Bean
	public Employee employee() {
		Employee employee = new Employee();
		employee.setEmpId(101);
		employee.setEmpName("Peter");
		employee.setEmpSal(10000);
		return employee;
	}

	@Bean
	DataSource dataSource() {
		DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();

		driverManagerDataSource.setUrl("jdbc:mysql://localhost:3306/mars_jan");

		driverManagerDataSource.setUsername("root");
		driverManagerDataSource.setPassword("Friend123");

		driverManagerDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");

		return driverManagerDataSource;

	}

}
