package com.training.spring;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ProductMapper implements RowMapper<Product> { // Interface

	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {

		Product prod = new Product();
		prod.setProdId(rs.getString(1));
		prod.setProdName(rs.getString(2));
		prod.setProdDesc(rs.getString(3));
		prod.setPrice(rs.getDouble(4));

		return prod;

	}

}
