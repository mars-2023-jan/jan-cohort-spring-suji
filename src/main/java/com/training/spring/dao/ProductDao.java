package com.training.spring.dao;

import java.util.List;

import com.training.spring.Product;

public interface ProductDao { // FETCH data from database

	Product getProductById(String id);

	List<Product> getAllProducts();

	void deleteProductById(String id);

	void updateProductById(String id, String desc);

	// Delete a product
	// Update a product

}
