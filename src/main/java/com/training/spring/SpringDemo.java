package com.training.spring;

import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.training.spring.config.ApplicationConfig;
import com.training.spring.dao.ProductDao;

public class SpringDemo {

	public static void main(String[] args) {

		ApplicationContext context = new ClassPathXmlApplicationContext("ApplicationConfig.xml");

		ApplicationContext javaContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);

		ProductDao dao = javaContext.getBean(ProductDao.class);
		List<Product> prodList = dao.getAllProducts();
		// System.out.println("List of all Product : ");

//		for (Product prod : prodList) {
//			System.out.println(prod); // print out the contents of a list of 'Product' objects to the console.
//		}

		Product product = dao.getProductById("EL105");
		System.out.println(product);

		dao.deleteProductById("KZ104");

		dao.updateProductById("KZ105", "LEGO");
		System.out.println(prodList);

		for (Product prod : prodList) {
			System.out.println(prod); // print out the contents of a list of 'Product' objects to the console.
		}

		// Employee emp = javaContext.getBean(Employee.class);
		// System.out.println(emp.getEmpName());
		// Person p1 = new Person();
		// Person p1 = (Person) context.getBean("person");

		// Person p2 = (Person) context.getBean("person");

		// p2.setFirstName("Kevin");

		// System.out.println("Person : " + p1.getPersonDetails());
		// System.out.println("Person : " + p2.getPersonDetails());
	}

}
