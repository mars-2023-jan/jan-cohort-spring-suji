package com.training.spring.dao;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.training.spring.Product;
import com.training.spring.ProductMapper;

//can be fetched as a bean or can autowire the class
@Component
public class ProductDaoImpl implements ProductDao {

	JdbcTemplate jdbcTemplate; // Help to Execute the queries

	private final String SQL_FIND_PRODUCT = "select * from product where product_id = ?";
	private final String SQL_GET_ALL = "select * from product";
	private final String SQL_DELETE = "delete from product where product_id = ?";
	private final String SQL_TO_UPDATE = "update product set product_desc = ? where product_id = ?";

	@Autowired // When a Spring bean is marked for autowiring, Spring will automatically
				// identify and inject the appropriate dependency at runtime.
	// When annotated Spring to automatically wire in the appropriate dependency for
	// that class or constructor.
	public ProductDaoImpl(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource); // Passing datasource will give the connection

	}

	public Product getProductById(String id) {
		return jdbcTemplate.queryForObject(SQL_FIND_PRODUCT, new Object[] { id }, new ProductMapper());
		// Data returned by query is converted in to object directly
	}

	public List<Product> getAllProducts() {
		return jdbcTemplate.query(SQL_GET_ALL, new ProductMapper());
	}

	public void deleteProductById(String id) {
		jdbcTemplate.update(SQL_DELETE, id);

	}

	public void updateProductById(String id, String desc) {
		jdbcTemplate.update(SQL_TO_UPDATE, desc, id);

	}

}
