package com.training.spring;

import org.springframework.beans.factory.annotation.Autowired;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Person {

	private String firstName;
	private String lastName;
	private int age;

	@Autowired
	private Car car;

	public Person(int age) {
		this.age = age;
	}

	public String getPersonDetails() {
		return firstName + " " + lastName + " : " + age + " Car Details  : " + car.getCarDetail();
	}

}
