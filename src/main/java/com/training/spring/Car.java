package com.training.spring;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Car {

	private String make;
	private String model;
	private int year;

	public String getCarDetail() {
		return make + "" + model + "" + year;
	}

}
